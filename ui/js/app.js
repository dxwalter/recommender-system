let postion = "Front-End Engineer";



// front-end
let css;
let javascript;
let html;

// back-end
let php;
let python;
let go;
let api;
let microservice;
let mqsql;


let lang;
let eduBackground;
let countryResidence;

let selectPosition = $('#selectPosition');
selectPosition = () => {
    let positionValue = $("#selectPosition option:selected").val();
    // Call tab function
    tabSection(positionValue);
}
// selectPosition.on('change', function() {

// })

let findFit = $('#findFit');
findFit.on('click', function() {
    alert("good");
})

let tabContainer = $('#tabContainer .tabContent');
tabSection = positionValue => {
	if(tabContainer.hasClass('active')) {
		tabContainer.removeClass('active');
		tabContainer.removeClass('fadeIn');
		conditionTab(positionValue.toLowerCase())
	} else {
		console.log("No")
	}
}

showTab = (positionValue) => {
	$('#tabContainer #' + positionValue).addClass('active fadeIn');
}

conditionTab = positionValue => {
	
	if (positionValue == "front-end") {
		postion = "Front-End Engineer";
		showTab(positionValue)
	}

	if (positionValue == "back-end") {
		postion = "Back-End Engineer";
		showTab(positionValue)
	}

	if (positionValue == "fullstack") {
		postion = "Fullstack Engineer";
		showTab(positionValue)
	}

	if (positionValue == "software") {
		postion = "Software Engineer";
		showTab(positionValue)
	}

	if (positionValue == "manager") {
		postion = "Manager";
		showTab(positionValue)
	}
}

$("#findCompany, #findCompanyTwo").click(function(e) {
  e.preventDefault();
  $("html,body").animate(
    {
      scrollTop: $("#mainAccess").offset().top
    },
    1300
  );
});

// THIS IS TO VALIDATE DATA GOTTEN FROM AN APPLICANT
// FOR FRONT-END APPLICATION

let frontEndBtn = $('#feSubmit');
frontEndBtn.on('click', function () {
	// collect in from front end
	collectFrontEndInput();
})


let backEndBtn = $('#backEndSubmit');
backEndBtn.on('click', function () {
	// collect in from front end
	collectBackEndInput();
});

let fullstack = $('#fullstackSubmit');
fullstack.on('click', function () {
	// collect in from front end
	collectFullStackInput();
});

validateHtmlDataInput = (inputData, alertMessage) => {
	if (inputData.lenght < 0) {
		// call bootstrap notification
		return;
	} else {
		return true;
	}
}

collectBackEndInput = () => {
	let php = $('#bkPhp').val();
	let python = $('#febkPython').val();
	let javascript = $('#bkJs').val();
	let webService = $('#BKwEBsERV').val();
	let dbase = $('#bkDb').val();
	let eduBackground = $('#EBbk').val();
	let language = $('#bkLg').val();
	let countryResidence = $('#bkCR').val();

	postion = "Back-End Engineer";

	// alert(html +" "+ css +" "+ javascript +" "+ eduBackground +" "+ language +" "+ countryResidence +" "+ postion)
	
	let beData = {
		inputData: `"[('php', ${php}), ('python', ${python}), ('javascript', ${javascript}), ('webservice', ${webService}), ('db', ${dbase}), ('education', ${eduBackground}), ('language', ${language}), ('residence', ${countryResidence})]"`	
	}

	httpRequest(beData);
}

collectFullStackInput = () => {
	
	let html = $('#fsHtml').val();
	let css = $('#fsCss').val();
	let javascript = $('#feJs').val();
	let php = $('#fsPhp').val();
	let python = $('#fsbkPython').val();
	let webService = $('#fswEBsERV').val();
	let dbase = $('#fsDb').val();
	let eduBackground = $('#fsbk').val();
	let language = $('#fsLg').val();
	let countryResidence = $('#fsCR').val();

	postion = "Fullstack Developer";

	// alert(html +" "+ css +" "+ javascript +" "+ eduBackground +" "+ language +" "+ countryResidence +" "+ postion)
	
	let fsData = {
		inputData: `"[('html', ${html}), ('css', ${css}), ('php', ${php}), ('python', ${python}), ('javascript', ${javascript}), ('webservice', ${webService}), ('db', ${dbase}), ('education', ${eduBackground}), ('language', ${language}), ('residence', ${countryResidence})]"`	
	}

	httpRequest(fsData);
}

collectFrontEndInput = () => {
	let html = $('#feHtml').val();
	let css = $('#feCss').val();
	let javascript = $('#feJs').val();
	let eduBackground = $('#feEB').val();
	let language = $('#feLg').val();
	let countryResidence = $('#feCR').val();

	// alert(html +" "+ css +" "+ javascript +" "+ eduBackground +" "+ language +" "+ countryResidence +" "+ postion)
	
	let feData = {
		inputData: `"[('html', ${html}), ('css', ${css}), ('javascript', ${javascript}), ('education', ${eduBackground}), ('language', ${language}), ('residence', ${countryResidence})]"`	
	}

	httpRequest(feData);
}


let httpRequest = (data) => {


	// alert('Enter your data');
	// return

	// show modal

	$('#hideLoader').fadeIn();
	$('#ajaxContent').fadeOut();
	$('#modalPosition').empty().html(postion)
	$('#resultModal').modal('toggle')

	$.ajax({
		type: "POST",
		url: "http://localhost:8000/recommend",
		crossDomain: true,
		dataType: "json",
		cache: false,
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		data: JSON.stringify(data),
		success: function(response) {
			callBackResponse(response)
		},
		error: function(response) {
			callBackResponse(response)
		}
	  });

}

let callBackResponse = (response) => {
	

		let responseData = response.responseText;
		$('#hideLoader').fadeOut();
		$('#ajaxContent').fadeIn();
		// change position 
		$('#choosenPosition').empty().html(postion)
		$('#compResult').empty().html(responseData)

}