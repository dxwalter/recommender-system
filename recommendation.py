# Data dictionary
from realdata import data 
import random
import json
import sys

# import math library
from math import sqrt

companyName = [
	"Cudua",
	"Facebook",
	"Google",
	"YouTube",
	"Microsoft"
]

newData = sys.argv[1];
data['userData'] = dict(eval(str(newData)))

 # Returns the Pearson correlation coefficient for p1 and p2
def sim_pearson(prefs,p1,p2):
 # Get the list of mutually rated items
 si={}
 for item in prefs[p1]:
	 if item in prefs[p2]: si[item]=1
 # Find the number of elements
 n=len(si)
 # if they are no ratings in common, return 0
 if n==0: return 0
 # Add up all the preferences
 sum1=sum([prefs[p1][it] for it in si])
 sum2=sum([prefs[p2][it] for it in si])
 # Sum up the squares
 sum1Sq=sum([pow(prefs[p1][it],2) for it in si])
 sum2Sq=sum([pow(prefs[p2][it],2) for it in si])
 # Sum up the products
 pSum=sum([prefs[p1][it]*prefs[p2][it] for it in si])
 # Calculate Pearson score
 num=pSum-(sum1*sum2/n)
 den=sqrt((sum1Sq-pow(sum1,2)/n)*(sum2Sq-pow(sum2,2)/n))
 if den==0: return 0
 r=num/den
 return r


def topMatches(prefs,person,n=1,similarity=sim_pearson):
 scores=[(similarity(prefs,person,other),other)
 for other in prefs if other!=person]
 # Sort the list so the highest scores appear at the top
 scores.sort( )
 scores.reverse( )
 return scores[0:n]
  # loop the score data, collect the employee name, use the name to get the company the employee works
 # check if the company already exists in the list. if the employee does, don't add to the new 
 # dictionary that will be created
  return scores[0:n]
  company = []
  for x in scores[0:n]:
   companyCode = data[x[1]]["company"]
   if companyCode not in company:
   	company.append(companyCode)

  return company


similarityResult = topMatches(data,'userData');

 computedCompany = []
 for companyCode in similarityResult:
 	computedCompany.append(companyName[companyCode])


print(str(computedCompany))
sys.stdout.flush()