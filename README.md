# recommender-system

THIS IS AN APPLICATION THAT COLLECTS
THE SKILL LEVEL OF AN INDIVIDUAL IN NUMERIC
QUANTITY (HTML: 3, CSS: 4, JAVASCRIPT: 2) AND 
PASSES IT AS AN INPUT PARAMETER TO GET SUGGESTION

# I USED SIM PEARSONS ALGORITHM FOR SIMILARITY CHECK.
# YOU CAN DOWNLOAD THE BOOK "PROGRAMMING COLLECTIVE INTELLIGENCE" TO FIND THE SOURCE CODE AND
# THE IMPLEMENTATION PROCESS


# YOU MUST HAVE PYTHON INSTALLED IN YOUR COMPUTER
# THIS COMMAND WILL RUN IN THE DIRECTORY THAT HAS RECOMMENDATION.PY file
# To run the python program copy the command below and paste into your command prompt

python recommendation.py "[('html', 2), ('css', 4), ('javascript', 7)]"

you can always modify the input parameter ('html': 5, 'css': 1, 'javascript': 4) to get a different result

