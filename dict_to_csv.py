from front_end import dict_data
import csv

csv_columns = ['html','css','javascript','company','oracle', 'php', 'go', 'mongodb', 'mysql', 'ruby', 'cassandra', 'python', 'api', 'microservice'
];

csv_file = "front_end.csv"

try:
    with open(csv_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for data in dict_data:
            writer.writerow(data)
except IOError:
    print("I/O error") 